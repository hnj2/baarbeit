%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}

\docStart

\section{Closed and closable operators}\label{sec:Close}
% mostly summarized from \autocite[][]{DWerner:FuAna}, ch. IV.4
%
This section summarizes some results from \autocite[IV.4, VII.2][343\psqq]{DWerner:FuAna}
that we will use to work with the differentiation operator $\deriv$, which
will be introduced later in section \ref{sec:closure_of_the_time_derivative}.

For any function $f$ we write $\dom(f)$ for the domain of $f$.
\begin{definition}[Closed operator, {\autocite[IV.4.1][156]{DWerner:FuAna}}]\label{def:closedOperator}
  Let $X$ and $Y$ be normed spaces,
  $D \subset X$ a subspace,
  $T\colon X \supset D \to Y$ linear.
  Then $T$ is called \newTerm{closed} if for every convergent sequence
  $(x_n)_{n ∈ ℕ}$, in $D$, $x_n \to x \in X$ with $Tx_n \to y \in Y$, we have $x \in D$ and $Tx = y$.
\end{definition}
%
Note that this is weaker than continuity.
Closedness of an operator $T$ can also be viewed as closedness of its graph
$\gr(T) = \setDef{(x, Tx)}{x \in D} \subseteq X \times Y$
(under the norm $\normiii {(x, y)} = \norm x + \norm y$) as one can check.
(See \autocite[IV.4.2][156\psq]{DWerner:FuAna}.)

\begin{definition}[Closable operator, Closure, {\autocite[VII.2.1][343]{DWerner:FuAna}}] \label{def:Closable}
  % from http://mathworld.wolfram.com/ClosableOperator.html
  An operator
  $T ⁚X ⊇ \dom(T) ⟶ Y$
  is called \newTerm{closable} if there exists a closed extension $B$ of $T$,
  that is $B ⁚ X ⊇ \dom(B) ⟶ Y$, such that $B$ is closed and $\dom(T) ⊆ \dom(B)$ and
  $\rest{B}{\dom(T)} = T$. If $B$ is the smallest closed extension,
  it is called the \newTerm{closure} of $T$ and $\gr(B) = \cl {\gr(T)}$ (closure in $X × Y$).
  Hence we write $B = \cl T$ for the closure.
\end{definition}

From now on in this chapter we only consider operators on Hilbert spaces.

Closely connected to the concept of closed operators are adjoint operators.
For continuous linear mappings $A \colon H → H$ on Hilbert spaces there
exist continuous adjoint operators $A^*$
that satisfy
\begin{equation*}
  ⟨Ax, y⟩ = ⟨x, A^*y⟩ \text{for all } x, y ∈ H.
\end{equation*}
In the case of (unbounded) operators the adjoint operator can still be
defined but one has to be careful about the domain.
%
\begin{lemma}[Well-definedness of the adjoint operator, {\autocite[VII.2][344]{DWerner:FuAna}}] \label{thm:defadjoint}
  Let $X, Y$ be Hilbert spaces and $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined operator.
  Then consider
  %
  \begin{align*}
    \dom(T^*) \definedas \setDef{y ∈ Y}{x ↦ ⟨Tx, y⟩ \text{ continuous on } \dom(T)}.
  \end{align*}
  %
  $\dom(T^*)$ is a linear subspace and for $y ∈ \dom(T^*)$
  we can extend $f \colon x ↦ ⟨Tx, y⟩$
  to a unique continuous linear functional on $X$.
  Furthermore, there is a unique $z ∈ X$ such that $⟨Tx, y⟩ = ⟨x, z⟩$
  for all $x ∈ \dom(T)$.
  We write $T^*y \definedas z$.
\end{lemma}
%
\begin{proof}
  Let $\hat x ∈ X$ and $(x_i)_{i ∈ ℕ}$ be a sequence in $\dom(T)$ that converges to
  $\hat x ∈ X$ and let $y ∈ \dom(T^*)$.
  In order to show that $f$ can be continuously extended to $X$,
  we want to show that $\lim_{i ⟶ ∞} ⟨Tx_i, y⟩$
  exists and is independent of the choice of the $x_i$.
  Consider
  %
  \begin{align*}
    \lim_{i, k ⟶ ∞} ⟨Tx_i, y⟩ - ⟨Tx_k, y⟩ = \lim_{i,k ⟶ ∞} ⟨T(x_i - x_k), y⟩ = 0
  \end{align*}
  %
  since $\dom(T) \ni x_i - x_k ⟶ 0$ and $f$ is continuous and $T$ and $⟨·,·⟩$ are linear.
  Therefore $(⟨Tx_i,y⟩)_i$ converges since it is a Cauchy sequence in $ℂ$.
  For uniqueness use the same argument for two different sequences in $\dom(T)$
  that both converge to $\hat x$.
  By construction the extension of $f$ is continuous on $Y$.
  %
  By the Fréchet-Riesz representation theorem
  (see \autocite[Theorem V.3.6][228]{DWerner:FuAna})
  and since $\dom(T)$ is dense in $X$ there is a unique $z ∈ X$ such that
  $⟨Tx, y⟩ = ⟨x, z⟩$ for all $x ∈ \dom(T)$.
\end{proof}
%
\begin{definition}[Adjoint operator, Self-adjoint, {\autocite[VII.2.3][344]{DWerner:FuAna}}] \label{def:adjoint}
  The operator described in lemma \ref{thm:defadjoint}
   %
  \begin{align*}
    T^* ⁚ Y \supseteq \dom(T^*) ⟶ X
  \end{align*}
  with
  \begin{align*}
    ⟨Tx, y⟩ = ⟨x, T^*y⟩ \quad \text{for all } x ∈ \dom(T), y ∈ \dom(T^*)
  \end{align*}
  %
  is the \newTerm{adjoint operator} of $T$.

  If $T = T^*$ (which implies $\dom(T) = \dom(T^*)$ and $X = Y$), we call
  $T$ \newTerm{self-adjoint}.
  $T$ is called \newTerm{skew-self-adjoint} if $\im T$ is self-adjoint.

  For a complex number $z \in ℂ$, $\conj z$ is the complex conjugate.
  This notation makes sense,
  since $\scaProd {zx, y} = \scaProd {x, z^*y}$ for $x, y$ elements of
  a complex Hilbert space.
\end{definition}
%
The adjoint operator has nice properties. First of all, it is closed:
\begin{lemma}[Closedness of $T^*$, {\autocite[VII.2.4 (a)][345]{DWerner:FuAna}}] \label{thm:adjointClosed}
  Let $X, Y$ be Hilbert spaces and $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined operator.
  Then the adjoint operator $T^*$ is closed.
\end{lemma}
\begin{proof}
  Let $(y_n)_{n ∈ ℕ}$ be a sequence in $\dom(T)$ with $y_n ⟶ y ∈ Y$ and $T^* y_n ⟶ \hat x ∈ X$ for $n ⟶ ∞$.
  Then for $x ∈ \dom(T)$
  %
  \begin{align*}
    ⟨Tx, y⟩ &\stackrel{(*)}= \lim_{n ⟶ ∞} ⟨Tx, y_n⟩ = \lim_{n ⟶ ∞} ⟨x, T^* y_n⟩\\ &= ⟨x, \hat x⟩ \stackrel{(*)}{⇒} x ↦ ⟨Tx, y⟩ \text { is continuous}
  \end{align*}
  %
  where in $(*)$ we have used the continuity of $⟨·,·⟩$.
  This means $y ∈ \dom(T^*)$ and $T^*y = \hat x$,
  since $⟨x, T^*y⟩ = ⟨x, \hat x⟩$ for all $x ∈ \dom(T)$ which is dense in $X$.
\end{proof}
%
Secondly, the closure can be characterised by applying $^*$ twice:
\begin{lemma}[Characterisation of closability {\autocite[Theorem 1.8][3]{Loss:closableOp}}] % {\autocite[VII.2.4 (b), (c)][345]{DWerner:FuAna}}, (similar statement)
  \label{thm:closability}
  Let $X, Y$ be Hilbert spaces and $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined operator.
  Then $\dom(T^*)$ is dense in $Y$ if and only if $T$ is closable.
  Furthermore, in this case $T^{**} = \cl T$ is the closure of $T$.
\end{lemma}
%
\begin{proof}
  For the easy direction assume that $T^*$ has dense domain.
  Then $T^{**}$ exists and
  for every $x ∈ \dom(T)$, $y ∈ \dom(T^{*})$, we have $⟨x, T^*y⟩ = ⟨Tx, y⟩$,
  thus $y ↦ ⟨T^*y, x⟩ = ⟨y, Tx⟩$ is continuous which means that $x ∈ \dom(T^{**})$ and $T^{**}x = Tx$.
  
  So $T⊆T^{**}$ and $T^{**}$
  is closed by the lemma \ref{thm:adjointClosed},
  hence $T^{**}$ is a closed extension of $T$.

  The other direction is more complicated. Assume that $T$ is closable.
  Since $T^* = \left(\cl T\right)^*$
  assume without loss of generality that $T$ is closed.
  It is to be shown that $\dom(T^*)$ is dense in $Y$. Take any $f ∈ \dom(T^*)^{\perp}$.
  We want to show that $f \neq 0$ implies that a sequence $(g_n)_{n ∈ ℕ}$ in $\dom(T)$
  exists
  such that $g_n ⟶ 0$ but $T g_n ⟶ f \neq 0$ (which would be a contradiction to
  the closability of $T$.)

  For this consider the minimization problem
  %
  \begin{align*}
    M = \inf_{g ∈ \dom(T)} \norm{f - Tg}^2 + \norm g ^2
  \end{align*}
  %
  (Note that the existence of such a sequence would imply $M = 0$.)

  The space $X \times Y$ with the inner product $⟨(f_1, g_1), (f_2, g_2)⟩ ≔ ⟨f_1, f_2⟩ + ⟨g_1, g_2⟩$
  is an Hilbert space and $\gr(T)$ is a closed subspace.
  By the projection theorem illustrated in figure \ref{fig:projectionTheorem}
  this infimum is assumed by an
  $(h, Th) ∈ \gr(T)$ such that
  \begin{equation*}
    M = \norm{(f, 0) - (Th, h)}^2 = \norm{f - Th}^2 + \norm h^2
  \end{equation*}
  \begin{figure}
    \centering
    \tikzinput{projectionTheorem}
    \caption{Projection theorem in $X \times Y$, see \autocite[Lemma 7.17][314\psq]{Alt:LinFuAna}}
    \label{fig:projectionTheorem}
  \end{figure}
  For any direction $v ∈ \dom(T)$ and scalar $t ∈ ℂ \without \{0\}$ this implies
  %
  \begin{align*}
    M &≤ \norm {f - T(h + tv)}^2 + \norm{h + tv}^2 \\
      &= \norm {f-Th}^2 + \norm{-tTv}^2 + 2 \Re ⟨f - Th, -tTv⟩
    + \norm h^2 + \norm {tv}^2 + 2 \Re ⟨h, tv⟩ \\
    ⇒ 0 &≤ \abs t^2 \left( \norm{Tv}^2 + \norm v^2 \right) +
    2 \Re \left(t \left( -⟨f-Th, Tv⟩ + ⟨h, v⟩\right)\right) \\
    &= \abs t \left ( \abs t \left( \norm {Tv}^2 + \norm v ^2\right) + 2 \Re \left (\frac t {\abs t}
      \left( - ⟨f-Th, Tv⟩ + ⟨h,v⟩ \right) \right) \right)
  \end{align*}
  %
  If $-⟨f - Th, Tv⟩ + ⟨h, v⟩$ was non-zero,
  one could choose $t ∈ ℂ$ such that
  \begin{equation*}
    \frac t{\abs t} \left( -⟨f - Th, Tv⟩ + ⟨h, v⟩\right) ∈ ℝ_{< 0}
  \end{equation*}
  % is real and negative
  and $\abs t$ small enough, such that this inequality is false. Hence
  %
  \begin{align*}
    ⟨h, v⟩ = ⟨f - Th, Tv⟩ \text{ for all $v ∈ \dom(T)$}.
  \end{align*}
  %
  This is the definition for $T^*(f - Th)$, that is
  \begin{equation}\label{eq:AdjointOfh}
    f - Th ∈ \dom(T^*) \text{ and } T^*(f - Th) = h.
  \end{equation}
  Since $f \perp \dom(T^*)$, we can conclude with the Cauchy-Schwarz inequality
  %
  \begin{equation} \label{eq:f<Th}
    ⟨f, f - Th⟩ = 0 ⇒ \norm f^2 = ⟨f, f⟩ = ⟨f, Th⟩ ≤ \norm f \norm {Th} ⇒ \norm f ≤ \norm {Th}.
  \end{equation}
  %
  Further
  %
  \begin{align*}
    \norm h^2 =
    &⟨h, h⟩ \stackrel{\eqref{eq:AdjointOfh}}=
    ⟨h, T^*(f - Th)⟩ =
    ⟨Th, f - Th⟩ =
    ⟨Th, f⟩ - \norm {Th}^2 =
    \norm f^2 - \norm{Th}^2 \\
    ⇒ &\norm h^2 + \norm{Th}^2 = \norm {f}^2 \stackrel{\eqref{eq:f<Th}}{≤} \norm {Th}^2 \\
    ⇒ &\norm h^2 ≤ 0
    ⇒ h = 0 ⇒ Th = 0 \\
    \stackrel{\eqref{eq:AdjointOfh}}{⇒} &f-Th = f ∈ \dom(T^*) ∩ \dom(T^*)^\perp ⇒ f = 0.
  \end{align*}
  This shows that $\dom(T^*)$ is dense in $Y$.

  Now we have $\cl T ⊆ T^{**}$ and want to show $T^{**} ⊆ \cl T$
  which means by definition that
  $\gr (T^{**}) ⊆ \cl {\gr(T)} ⊆ X × Y$
  as mentioned in definition \ref{def:Closable}.

  As before we use the inner product $⟨ (u,v), (x,y) ⟩ \definedas ⟨ u,x ⟩ + ⟨ v,y ⟩$ on $X \times Y$.
  For $\cl {\gr(T)} ⊇ \gr (T^{**})$ it is enough to show that
  $ {\gr(T)}^{\perp} ⊆ \gr (T^{**})^{\perp}$.
  So let $(u,v) ∈ \gr (T)^{\perp}$, i.\,e.\ $⟨x,u⟩ + ⟨Tx, v⟩ = 0$ for all $x ∈ \dom(T)$.
  Then $x ↦ ⟨Tx, v⟩ = ⟨x, -u⟩$ is continuous,
  hence $v ∈ \dom(T^*)$ and $T^* v = -u$.
  For $(z, T^{**}z) ∈ \gr(T^{**})$ we then have
  %
  \begin{align*}
    ⟨(z, T^{**}z), (u,v)⟩ &= ⟨z, u⟩ + ⟨T^{**} z, v⟩ = ⟨z,u⟩ + ⟨z, T^*v⟩ \\
    &= ⟨z, u + T^* v ⟩ = ⟨z, 0⟩ = 0 \\
    &⇒ (z, T^{**}z) \perp {\gr(T)}^{\perp}
  \end{align*}
  %
  This is what we wanted to show.
\end{proof}
In the following we consider operators from one Hilbert space to itself and therefore call this Hilbert space $H$.

An important kind of operators are the symmetric ones:
\begin{definition}[Symmetric operator, {\autocite[VII.2.2][344]{DWerner:FuAna}}] \label{def:symmOp}
  Let $T ⁚ H ⊃ \dom(T) ⟶ H$ be a densely defined operator with
  $⟨Tx, y⟩ = ⟨x, Ty⟩$ for all $x,y ∈ \dom(T)$.
  Then $T$ is called \newTerm{symmetric}.
\end{definition}
%
This definition of symmetry appears to be almost the same as self-adjoint but the definition ranges (here $x,y ∈ \dom(T)$, for $T = T^*$, $\dom(T) = \dom(T^*)$ is required) are different. That's why those two terms should not be confused. (Also see \autocite[Ch.~9][169\psqq]{Hall:2009}.)
%
\begin{lemma}[Symmetric operators are closable, {\autocite[Proposition~9.4][171]{Hall:2009}}] \label{thm:ClosureOfSymmetricInAdjoint}
  Let $T ⁚ H ⊃ \dom(T) ⟶ H$ be a symmetric operator.
  Then $T$ is closable with $\cl T ⊆ T^*$.
\end{lemma}
%
\begin{proof}
    Let $y ∈ \dom(T)$.
    Then $x ↦ ⟨Tx, y⟩ = ⟨x, Ty⟩$ is clearly continuous,
    since $⟨·,·⟩$ is continuous and for all $x ∈ \dom(T)$, $⟨x, Ty⟩ = ⟨Tx, y⟩ = ⟨x, T^*y⟩$ holds.
    Hence $T ⊆ T^*$.
    In every metric space we have for two sets $A ⊆ B ⇒ \cl A ⊆ \cl B$,
    so
    $\cl {\gr (T)} ⊆ \cl {\gr (T^*)} \stackrel{\ref{thm:adjointClosed}}= \cl {T^*}$
    and with definition \ref{def:Closable} this is
    $\cl T ⊆ T^*$.
\end{proof}

\docEnd
