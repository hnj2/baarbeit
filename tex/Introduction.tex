%! TEX program = lualatex
\input{.maindir/tex/header/preamble-section}

\docStart

\section{Introduction} \label{sec:introduction}
\subsection{Goal} \label{sec:goal}
Teaching an analysis course for undergraduate students about ordinary differential
equations usually includes some solution techniques for special types of equations that
can be solved explicitly together
the existence theorem of Peano and the existence
and uniqueness theorem of Picard-Lindelőf.
Both give local results and give differentiable solutions. Furthermore
the proofs are technical in the sense that the treatment of the correct
definition ranges of the solution obfuscate the central idea of the proof
which is -- for Picard-Lindelőf -- a contraction argument.

Those direct ideas did not prove sufficient for tackling partial differential equations.
While an unified solution theory for PDE does not exist many approaches use functional
analysis and solution theory in the weak sense. Those can help in many cases
where direct solving is not possible and provide a unified way of solving
broad classes of equations.

Applying the functional analysis to ordinary differential equations might appear
to be unnecessary complicated but there are several reasons why it might be a reasonable
alternative that can be taught in a seminar.
%
\begin{enumerate}
  \item As just noted being firm in functional analysis and solving differential equations
    in a weak sense is a strong background for partial differential equations afterwards.
  \item Since all the theory about Hilbert space operators can be treated on its own
    the central contraction idea can stand out in its simplicity.
  \item The classical versions of Peano and Picard-Lindelőf only consider
    classical differential equations where the solution and the right-hand side are
    sufficiently smooth. A wide range of applications is not covered by this though.
    One big class of ordinary differential equations
    that are studied are delay equations. Adjusting Picard-Lindelőf to
    those cases is not trivial while in the setting regarded in \autocite[][]{TUD:HSDDE}
    those are just a special case and were the main motivation for the paper in the first place.
  \item As mentioned above classical solution theory is usually local and global
    solution theory is based on the local results. Here we will only regard
    global solutions, that is we always regard functions that are defined on
    the entire real line. In \autocite[section 5.2][28 ff.]{TUD:HSDDE} an
    application to local problems is given but this will be beyond the scope
    of this paper.

    With global in time solutions comes the notion of causality that
    captures the idea that a solution up to any time cannot depend on the future
    but only on the past and present. Hence conditions on the differential equation
    are presented to ensure causal solution operators.
\end{enumerate}
%
My aim is to give the reader a smooth guide to the outlined solution theory.
This thesis is mostly self-contained, presenting just the parts and cases of the
general theory used afterwards. My goal is to present not only %correct
statements
and proofs but also ideas on why the regarded theory is developed and how
one can go about proving the statements. I filled the gaps I found myself stuck in
during studying the paper \autocite[][]{TUD:HSDDE} this thesis is based on and hope that in this way
others can easily follow the presented thoughts.
To support this connection to \autocite[][]{TUD:HSDDE} references
to the respective sections are given throughout.

In order to limit the extend of this thesis some topics of
\autocite[][]{TUD:HSDDE} are left out. They are not essential to the central
solution theory, namely Fourier transformation (\autocite[Section 2][7\psq]{TUD:HSDDE}),
functional calculus (\autocite[Definition 2.10 and following][11\psq]{TUD:HSDDE})
and its applications (\autocite[Section 5.3.2][34\psq]{TUD:HSDDE}),
the continuous dependence on the data.
(\autocite[Theorem 3.6][15\psq]{TUD:HSDDE}), initial value problems
(\autocite[Section 5.1][24\psqq]{TUD:HSDDE}) and local solvability
(\autocite[Section 5.2][28\psqq]{TUD:HSDDE}).
%
\subsection{Structure} \label{sec:structure}
Based on the work of the previous chapters the solution theory is presented
in section \ref{sec:basic_solution_theory}.
At first the existence and uniqueness of solutions is discussed.
Then in section \ref{sec:causality} the concept of causality is
introduced.% already in Goal section: that describes that the
% solution of a differential equation only depends on the equation
% in the past.

A solution theory for (differential)
equations has to analyse conditions on the equation that can guarantee
the existence and uniqueness of a solution in a set of solution candidates.
The candidates, so called extrapolation spaces, are a variant of $\LpD$-spaces
and are introduced in section \ref{sec:extrapolation_spaces} and
\ref{sec:gelfand_triple}.

Since it is no more work to consider Hilbert space valued
functions than $ℂ$ valued functions we introduce the extrapolation spaces
consisting of Hilbert space valued functions. The background is
presented in section \ref{sec:tensorproduct}.

Since $\LpD$
includes non-differentiable functions the question arises what differentiation
as an operator on $\LpD$ and extrapolation spaces means.
For this purpose we introduce differentiation as a closable operator
between Hilbert spaces.
Hence the needed aspects on closure
of (unbounded) linear operators
are introduced in section \ref{sec:Close}.

After the general theory some special cases are looked at that
can successfully be tackled by the developed solution theory including
ordinary differential equations (section \ref{sec:nemitzki}),
delay differential equations with discrete (section \ref{sec:discreteDelay}) and
continuous delay (section \ref{sec:continuousDelay}).
\docEnd
