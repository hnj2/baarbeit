%! TEX program = lualatex
\input{.maindir/tex/header/preamble-section}

\docStart

\section{Extrapolation spaces}
\label{sec:extrapolation_spaces}
\begin{textfold}[introduction]
  As it was mentioned in the introduction (section \ref{sec:goal})
  we want to work with Hilbert space valued functions, not only
  $ℂ$-valued ones. 
  Fortunately it does not hurt to always think of
  $ℂ$-valued functions since the theory does not change thanks to
  the results of section \ref{sec:tensorproduct}.

  Let $H$ be any Hilbert space throughout the remaining thesis.
\end{textfold}
%
\subsection{Antiderivatives of test functions} \label{sec:antiderivativesOnTst}
\begin{textfold}[weighted test function introduction]
  In order to write a differential equation as a fixed point problem as we
  will to in section \ref{sec:basic_solution_theory} we need to study the
  inverse of the differentiation operator $\deriv$.

  Since $\deriv$ is the usual derivative on test functions $φ ∈ \tstH$,
  the inverse $\deriv^{-1} φ$ must be an antiderivative, given
  by $∫_{x_0}^x φ(t) \D t$ for some $x_0 ∈ ℝ$. Since constant functions apart from $0$ are not
  in $\LpH{H}$, at most one antiderivative $Φ$ is in $\LpH{H}$.

  $Φ$ must be constant for sufficiently small and large ($ℝ \without \supp (φ)$) arguments,
  call these values $Φ(-∞)$ and $Φ(∞)$.
  They are related via $Φ(∞) = Φ(-∞) + ∫_{ℝ} φ$ by the fundamental theorem of calculus.

  Unfortunately this implies that $Φ$ is not in $\LpH{H}$ except for the special case
  $∫_{ℝ} φ = 0$. Hence we need a different space that contains functions
  that are constant from some point onward. Since $\deriv$ needs to be defined for
  those functions as well we can inductively conclude that one of the following
  two function spaces must be included in our setting:
\end{textfold}
%
\begin{definition}[Test space, {\autocite[Definition 3.1][12]{TUD:HSDDE}}] \label{def:C+}
  Define
  %
  \begin{equation*}
    \tstp+ \definedas
    \setDef{ϕ ∈ \tstH}
    {
      \begin{aligned}
        &\sup \supp ϕ < ∞ \\
        &\text{and there is } n ∈ ℕ \text{ with } ϕ^{(n)} ∈ \tstH
      \end{aligned}
    }
  \end{equation*}
  %
  and
  %
  \begin{equation*}
    \tstp- \definedas
    \setDef{ϕ ∈ \tstH}
    {
      \begin{aligned}
        &\inf \supp ϕ > ∞ \\
        &\text{and there is } n ∈ ℕ \text{ with } ϕ^{(n)} ∈ \tstH
      \end{aligned}
    }
  \end{equation*}
  %
  (The $+$/$-$ in the notation should hint to the fact, that the compact support only
  applies to the positive/ negative side of the real line.)
\end{definition} % end definition of name
%
\subsection{Weighted \texorpdfstring{$\LpD$}{L2}-space} \label{sec:weightedspace}
In order to apply methods of functional analysis we want to work in a Hilbert space
similar to $\LpH{H}$. In order to include $\tstpD{\pm}(ℝ)$ we introduce an exponential weight.
%
\begin{definition}[weighted Space, {\autocite[Def.~2.3]{TUD:HSDDE}}] \label{def:weightedSpace}
  Let $\w ∈ ℝ$. Define
  \begin{align*}
    \Hi {\w} 0 &\definedas
    \setDef{f ∈ \Lploc{ℝ}}{(x ↦ \exp(- \w x) f(x)) ∈ \Lp{ℝ}} \\
    \Hi {\w} 0 \tens H &= 
    \setDef{f ∈ \Lploc{ℝ} \tens H}{(x ↦ \exp(- \w x) f(x)) ∈ \Lp{ℝ} \tens H}
  \end{align*}
  We endow $\Hi {\w} 0$ and $\HiH{\w}0$ respectively with the inner products
  \begin{align*}
    (f, g) ↦ ⟨f, g⟩_{\w, 0} & \definedas ∫_{ℝ} f(x)\conj{g(x)} \exp(-2\w x) \D x \\
    (f, g) ↦ ⟨f, g⟩_{\w, 0} &= ∫_{ℝ} ⟨f(x), g(x)⟩_H \exp(-2\w x) \D x
  \end{align*}
  and the induced norms, both called $\norm{·}_{\w, 0}$.
  Here $\Lploc{ℝ}$ is the space of all
  locally square-integrable functions
  \begin{align*}
    \Lploc{ℝ}
    &= \setDef{f ⁚ ℝ ⟶ ℂ}
    {f \text{ measurable, } ∀ K ⊂ ℝ \text{ compact }
    ∫_K \norm{f(x)}^2 \D x < ∞}%/=_{\text{a.e.}}
  \end{align*}
  (of course factored out by equality almost everywhere).

  Note that $\Hi{0}0 = \Lp{ℝ}$, $\HiH00 = \Lp{ℝ} \tens H$ and
  $\norm{·}_{0,0} = \norm{·}_{\Lp{ℝ}}$.
\end{definition}
%
\begin{note}[Unitary $\exp(-\w \mo)$, {\autocite[after Definition~2.3][8]{TUD:HSDDE}}] \label{note:defexp}
  $\HiH{\w}0$ is obviously isometrically isomorphic to $\LpH{H}$ with the
  following unitary operator $\exp(-\w \mo )$:
  \begin{equation}
    \exp(-\w \mo ) ⁚ \Hi {\w} 0 \tens H ⟶ \Lp{ℝ} \tens H⁚ f ↦ (x ↦ \exp(- \w x) f(x)).
    \label{eq:defexp}
  \end{equation}
  with $\mo$ being multiplication with the argument:
  % not call it operator since we do not introduce it properly as an operator
  \begin{equation*}
    (\mo f) (x) = xf(x) \quad (x ∈ ℝ).
  \end{equation*}
  Here $\exp$ is \imp{not} to be understood pointwise:
  \begin{equation*}
    \exp(-\w \mo )(f)(x) \neq \exp(-\w (\mo f) (x)) = \exp(-\w x f(x)).
  \end{equation*}
  Instead remember the definition of $\exp$ on $ℝ$: $\exp(x) = Σ_{k=0}^{∞}\frac{x^k}{k!}$.
  Here $\exp$ is applied to the linear operator $-\w \mo $
  and the multiplication used to define the powers of the argument
  is concatenation. For $f ∈ \Hi {\w} 0$, $x ∈ ℝ$
  \begin{align*}
    \exp(-\w \mo ) &= Σ_{k=0}^{∞} \frac {(-\w \mo )^k}{k!} \\
    ⇒ \exp(-\w \mo ) (f) &= Σ_{k=0}^{∞} \frac 1{k!} (-\w \mo )^k(f) \\
    ⇒ \exp(-\w \mo ) (f) (x) &= Σ_{k=0}^{∞} \frac 1{k!} (-\w \mo )^k(f)(x)
    = Σ_{k=0}^{∞} \frac 1{k!} (-\w)^kx^kf(x) \\
    &= \left(Σ_{k=0}^{∞} \frac 1{k!} (-\w)^kx^k \right)f(x)
    = \exp(-\w x) f(x).
  \end{align*}
  %
  This is also the canonical generalisation of $\exp$ used on matrices where
  concatenation and (matrix) multiplication coincide.
\end{note} % end note Unitary $\exp(-\w \mo)$
%
On $\Lp{ℝ}$ (and $\Lp{ℝ} \tens H$)
we developed the differentiation as a linear operator with the greatest possible domain
in section \ref{sec:closure_of_the_time_derivative}.
In order to expand this operator to the extrapolation spaces
we can use the unitary operators
$\exp(-\w m)$ to go from $\HiH{\w}0$ to $\Lp{ℝ} \tens H$,
differentiate there and go back:
%
\begin{definition}[Naive differentiation on weigted space, {\autocite[Corollary 2.5][8]{TUD:HSDDE}}]\label{def:NaiveDifferentiationOnWeigtedSpace}
  For $\w ∈ ℝ$ define the (unbounded) linear operator $\dernaive $ on
  \begin{equation}
    \begin{split}
      \dom(\dernaive ) &= \exp(-\w \mo)^{-1}(\dom(\deriv))\subset \HiH{\w}0 \\
      \dernaive  &\colon \dom(\dernaive) → \HiH{\w}0 \\
      \dernaive  &\definedas \exp(-\w \mo)^{-1} \deriv \exp(-\w \mo).
    \end{split}
  \end{equation}
\end{definition} % end definition of Naive differentiation on weigted space
%
After defining $\dernaive $ one can ask if that is what we would expect
of the derivative in the cases that we can calculate directly: $\tst$.
Let $φ ∈ \tst ⊆ \Hi{\w}{0}$. Then
\begin{equation}
  \label{eq:correctiontermdifferentiation}
  \begin{split}  
    (\dernaive  f)(x) &= \exp(-\w m)^{-1} \deriv \big(x ↦ \exp(-\w x) f(x)\big) \\
                      &= \exp(-\w m)^{-1} \big(x ↦ -\w \exp(-\w x) f(x) + \exp(-\w x) f'(x)\big) \\
                      &= \exp(-\w m)^{-1} \exp(-\w m) \big(x ↦ -\w f(x) + f'(x)\big) \\
                      &= -\w f + f'
    % = (\deriv - \w)f nicht schreiben, weil f gerade noch als Element von \Hi{\w}{0} aufgefasst worden war
  \end{split}
\end{equation}
This is not exactly what we desired since generalised differentiation
should be the classical derivative on
smooth functions. That is why we introduce a correction term.
\begin{definition}[Differentiation on weigted space, {\autocite[Corollary 2.5][8]{TUD:HSDDE}}] \label{def:DifferentiationOnWeightedSpace}
  For $\w ∈ ℝ$ define
  \begin{equation}
    \der \definedas \dernaive  + \w.
  \end{equation}
\end{definition} % end definition of Differentiation on 
%
The calculation in \ref{eq:correctiontermdifferentiation} also verifies that
$\der$ does not depend on $\w$
except for the domain. (Also see \autocite[after Corollary 2.5][9]{TUD:HSDDE}.)

So far all we achieved is an expansion of our space of functions
but the actual gain is that
differentiation becomes an invertible operator on $\HiH{\w}0$ for $\w \neq 0$.
Here we use lemma \ref{thm:Real_spectrum_of_self-adjoint_operators}:
$\w ∈ ℝ$ and therefore $\w$ is not in the spectrum of $\dernaive $.
\begin{corollary}[$\der$ continuously invertible]\label{thm:diffContinuouslyInvertible}
  For $\w ∈ \R_{\neq 0}$ the previously in \ref{def:DifferentiationOnWeightedSpace}
  defined operator $\der$ has a continuous inverse with
  \begin{equation*}
    \opnorm{\der^{-1}}{\HiH{\w}0} = \frac 1{\abs{\w}}.
  \end{equation*}
\end{corollary} % end corollary $\der$ continuously invertible
\begin{proof}
  By lemma \ref{thm:dSelfadjoint} $\im \deriv$ is self-adjoint and hence
  by lemma \ref{thm:Real_spectrum_of_self-adjoint_operators} the
  spectrum of $\im \deriv$ is contained in the real axis. This means by definition,
  that $\im \deriv - \im \w$ is continuously invertible for all $\w ∈ ℝ_{\neq 0}$
  and lemma \ref{thm:Real_spectrum_of_self-adjoint_operators} gives
  the estimate
  \begin{equation*}
    \opnorm{(\deriv-\w)^{-1}}{\Lp{ℝ}} = \opnorm{\im(\deriv - \w)^{-1}}{\Lp{ℝ}}
    \geq \frac 1{\abs{\w}}.
  \end{equation*}
  In order to show $\opnorm{\im(\deriv - \w)^{-1}}{\Lp{ℝ}} \leq \frac 1{\abs{\w}}$
  we could find an element $f ∈ \Lp{ℝ}$ such that
  $\norm{(\deriv - \w)^{-1}f}_{0, 0} = \frac 1{\abs{\w}}$ or
  equivalently a $g ∈ \Lp{ℝ}$ such that $\deriv g = 0$. The typical candidates
  would be constant functions but those are not in $\Lp{ℝ}$, so
  we need to find an approximating sequence. Define
  \begin{equation*}
    g_n \definedas \frac{δ * \chFct_{[-n, n]}}{\norm{δ * \chFct_{[-n, n]}}_{0, 0}}
  \end{equation*}
  with $δ = δ_1$ being a mollifier
  as defined in definition \ref{def:delta_fct}.
  Then $g_n ∈ \tst$ and hence we can easily calculate
  \begin{align*}
    \norm{\deriv(g_n)}_{0,0} = \norm{g_n'}_{0,0} \stackrel{\ref{thm:ConvolutionInTestfcts}}=
    \norm{\frac{δ' * \chFct_{[-n, n]}}{\norm{δ*\chFct_{[-n, n]}}_{\Lp{ℝ}}}}_{0, 0}
    = \underbrace{\norm{δ' * \chFct_{[-n, n]}}_{0,0}}_{\text{constant,} n → ∞}
    \underbrace{\norm{δ*\chFct_{[-n, n]}}_{0,0}^{-1}}_{→ 0,\, n → ∞}
    → 0
  \end{align*}
  $δ'$ is an function with support in $[-1, 1]$ and integral $0$. Hence
  % $\rest{δ' * \chFct_{[-n, n]}}{(-∞, -n - 1) ∪ (-n + 1, n - 1) ∪ (n + 1, ∞)} = 0$
  % and on the remaining domain
  $δ' * \chFct_{[-n, n]}$ consists
  of two bumps on $[-n-1, -n + 1]$ and $[n-1, n + 1]$
  that move to $\pm ∞$ for $n → ∞$ but look the same for all $n$.
  Hence $\norm{δ' * \chFct_{[-n, n]}}_{0,0}$ is constant
  over all $n ∈ ℕ$.

  This tells us
  \begin{align*}
    \norm{(\deriv - \w)g_n}_{\Lp{ℝ}} \leq \norm{\deriv g_n}_{0,0} + \abs{\w}\norm{g_n}_{0,0} → \abs{\w}\norm{g_n}_{0,0}, \\
    \text{hence }
    \opnorm{(\deriv -\w)^{-1}}{\Lp{ℝ}} \leq \frac 1 {\abs{\w}}.
  \end{align*}
  Since $\exp(-\w \mo)$ is unitary,
  $\opnorm{\der^{-1}}{\w, 0} = \opnorm{(\deriv + \w)^{-1}}{\Lp{ℝ}} = \frac 1{\abs{\w}}$.

  $\der^{-1}$ on $\HiH{\w}0$ has the same norm as discussed in example \ref{ex:AtensId}.
\end{proof}
% todo: irgendwie motivieren , z.B. anhand von Beispiel, warum das toll ist -> ODE im allg. lösbar! (Picard-Lindelőf mit anderen (schwächeren?) Voraussetzungen)

Now that we have proper notation for the inverse of differentiation we
can summarize the conclusions of section \ref{sec:antiderivativesOnTst}:
%
\begin{corollary}[Explicit formula for antiderivatives, {\autocite[Corollary 2.5 (d)][8]{TUD:HSDDE}}] \label{thm:ExplicitAntiderivativeFormula}
  Let $\w ∈ ℝ_{> 0}$, $φ ∈ \tstp- \subset \HiH{\w}0$. Then
  \begin{equation} \label{eq:integralposw}
    \left(\der^{-1} φ\right) (x) = ∫_{-∞}^x φ(t) \D t = - ∫_x^{∞} φ(t) \D t.
  \end{equation}
  Let $φ ∈ \tstp+ \subset \HiH{-\w}0$. Then
  \begin{equation} \label{eq:integralnegw}
    \left(\der[-\w]^{-1} φ\right) (x) = ∫_{∞}^x φ(t) \D t = - ∫_x^{∞} φ(t) \D t
  \end{equation}
\end{corollary} % end corollary Explicit formula for antiderivatives
%
\docEnd
